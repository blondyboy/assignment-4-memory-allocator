#include "test.h"

static void print_newline() {
    printf("\n");
}
static void print_line(){
    printf("--------------------");
}

static void print_status(char* const message) {
    print_line();
    printf("%s", message);
    print_line();
    print_newline();
    print_line();
    print_line();
    print_newline();
}

static void free_heap(void* addr, size_t size) {
    munmap(addr, size_from_capacity((block_capacity) { size }).bytes);
}


static void print_test_status(bool test, int number) {
    if (test) {
        printf("✓ %d Test succecesfuly complite ", number);
    } 
    else {
        printf("x %d Test was failed", number);
    }
    print_newline();
}

static bool test1() {
    print_status("Test for checking memory allocation");
    size_t heap_size = 8192;
    void* init_heap = heap_init(8192);
    if(!init_heap) {
        print_status("Heap wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }
    print_status("Heap was allocated succesesful");
    debug_heap(stdout, init_heap);
    void* malloc = _malloc(54);
    if (!malloc) {
        print_status("Memory wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }
    print_status("Memory was allocated");
    debug_heap(stdout, init_heap);
    _free(heap_init);
    print_status("Memory was been cleared");
    free_heap(heap_init, 8192);
    print_status("Test was succesfull");
    return true;
}

static bool test2() {
    print_status("Test for checking memory allocation");
    size_t heap_size = 8192;
    void* init_heap = heap_init(heap_size);
    if(!init_heap) {
        print_status("Heap wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }
    print_status("Heap was allocated succesesful");
    debug_heap(stdout, init_heap);
    void* malloc0 = _malloc(54);
    void* malloc1 = _malloc(102);
    void* malloc3 = _malloc(1024);

    if (!malloc0 || !malloc1 || !malloc3) {
        print_status("Memory wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }


    print_status("Memory was allocated");
    debug_heap(stdout, init_heap);
    _free(malloc0);
    print_status("First block was released");
    debug_heap(stdout, init_heap);
    _free(malloc1);
    print_status("Second block was released");
     debug_heap(stdout, init_heap);
    _free(malloc3);
    print_status("Third block was released");
    print_status("Memory was been cleared");
    debug_heap(stdout, init_heap);
    free_heap(init_heap, heap_size);
    print_status("Test was succesfull");
    return true;

}

static bool test3() {
    print_status("Test for checking memory allocation");
    size_t heap_size = 8192;
    void* init_heap = heap_init(heap_size);
    if(!init_heap) {
        print_status("Heap wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }
    print_status("Heap was allocated succesesful");
    debug_heap(stdout, init_heap);
    void* malloc0 = _malloc(54);
    void* malloc1 = _malloc(102);
    void* malloc3 = _malloc(1024);
    void* malloc4 = _malloc(1024);
    if (!malloc0 || !malloc1 || !malloc3 || !malloc4) {
        print_status("Memory wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }
    print_status("Memory was allocated");
    debug_heap(stdout, init_heap);
    _free(malloc0);
    _free(malloc4);
    debug_heap(stdout, init_heap);
    _free(malloc1);
    _free(malloc3);
    print_status("Memory was been cleared");
    free_heap(init_heap, heap_size);
    print_status("Test was succesfull");
    return true;
}

static bool test4() {
    print_status("Test for checking memory allocation");
    size_t heap_size = 8192;
    void* init_heap = heap_init(heap_size);
    if(!init_heap) {
        print_status("Heap wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }
    print_status("Heap was allocated succesesful");
    debug_heap(stdout, init_heap);
    void* malloc0 = _malloc(4096);
    debug_heap(stdout, init_heap);
    if (!malloc0) {
        print_status("First block was initalizated");
        free_heap(init_heap, heap_size);
        return false;
    }
    
    void* malloc1 = _malloc(8192);
    debug_heap(stdout, init_heap);
    if (!malloc0 || !malloc1) {
        print_status("Second block was initalizated");
        free_heap(init_heap, heap_size);
        return false;
    }
    debug_heap(stdout, init_heap);
    _free(malloc0);
    _free(malloc1);
    print_status("Memory was been cleared");
    free_heap(init_heap, heap_size);
    print_status("Test was succesfull");
    return true;
}

static bool test5() {
    print_status("Test for checking memory allocation");
    size_t heap_size = 8192;
    void* init_heap = heap_init(heap_size);
    if(!init_heap) {
        print_status("Heap wasn't initalizated. Refactor your code and try again");
        return false;
    }
    print_status("Heap was allocated succesesful");
    debug_heap(stdout, init_heap);
    void* malloc0 = _malloc(8192);
    
    if (!malloc0) {
        print_status("Memory wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
        return false;
    }

    struct block_header* header = (struct block_header*)(malloc0 - offsetof(struct block_header, contents));
    void* buff = mmap((void*)(header->contents + header->capacity.bytes), 2 * 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    if (!buff) {
		print_status("Buffer head was not initialized");
        free_heap(init_heap, heap_size);
		return false;
	}
    void* malloc1 = _malloc(2048);
	if (!malloc1) {
		print_status("Memory wasn't initalizated. Refactor your code and try again");
        free_heap(init_heap, heap_size);
		return false;
	}
    free_heap(init_heap, heap_size);
	free_heap(buff, heap_size);
	_free(malloc0);
	_free(malloc1);
    print_status("Test was succesfull");
    return true;


}

bool start_test() {
    bool test_1 = test1();
    bool test_2 = test2();
    bool test_3 = test3();
    bool test_4 = test4();
    bool test_5 = test5();
    print_test_status(test_1, 1);
    print_test_status(test_2, 2);
    print_test_status(test_3, 3);
    print_test_status(test_4, 4);
    print_test_status(test_5, 5);
    if (test_1 && test_2 && test_3 && test_4 && test_5) {
        return true;
    }
    return false;
}