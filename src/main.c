#include "test.h"

int main() {
    bool res = start_test();
    if (res) {
        printf("\n%s","All tests have been successfully passed");
    } else {
        printf("\n%s", "Oops...we had some problems with our programm...");
    }
    return 0;
}